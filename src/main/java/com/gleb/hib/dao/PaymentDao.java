package com.gleb.hib.dao;

import com.gleb.hib.model.Payment;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository
public class PaymentDao {
    
    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(propagation = Propagation.MANDATORY)
    public void createPayment(Payment payment) {
        entityManager.persist(payment);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public Payment getPayment(Long id) {
        return entityManager.find(Payment.class, id);
    }

    @Transactional(propagation = Propagation.MANDATORY)
    public void updatePayment(Payment order) {
        entityManager.merge(order);
    }

    @Transactional
    public List<Payment> findAllByStatus(String statusName) {
        return entityManager.createNamedQuery("findPaymentByStatus", Payment.class)
                .setParameter("status", statusName).getResultList();
    }
}
